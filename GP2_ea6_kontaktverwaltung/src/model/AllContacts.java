package model;


import java.util.ArrayList;
import java.util.List;


/**
 * 
 * @author Marc André Fischer
 *
 * This is a Resource that returns an amount of contacts. In this application
 * it can be seen as a replacement of other sources like files or a database.
 * @return a List of Contacts
 */
public class AllContacts {
	
	/**
	 * 
	 */
	private static List<Contact> contacts;
	
	/**
	 * 
	 * @return List of Contacts
	 * 
	 */
	public static List<Contact> getContactList() {
		contacts = new ArrayList<Contact>();
		
		contacts.add(
				new Contact("Mathias", "Trappe", 
					new PhoneNumber("0157/66596485"), 
					new PhoneNumber("017265887465"),
					new PhoneNumber("07154/ 286493")
				));
		
		contacts.add(
				new Contact("Marc", "Meier", "../resources/marc-meier.jpg", "marc.meier@hotmail.de",
					new PhoneNumber("0711/ 546587"), 
					new PhoneNumber("0174/ 65987125"),
					new PhoneNumber("12468797854465")
				));
		

		contacts.add(
				new Contact("Marcel", "Frisch", "../resources/marcelfrisch.jpg", "marcelfirsch@gmail.com",
						new PhoneNumber("015799457213"),
						new PhoneNumber("0162754136958")
				));
		
		contacts.add(
				new Contact("Michael", "Werner", 
					new PhoneNumber("07074564594"),
					new PhoneNumber("0702185794586"),
					new PhoneNumber("017265975894"),
					new PhoneNumber("0157/66596485")
				));
	
		return contacts;
	}

}
