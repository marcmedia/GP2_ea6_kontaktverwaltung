package model;


/**
 * 
 * @author Marc
 * 
 * Java Bean that represents a Phonenumber.
 *
 *
 */
public class PhoneNumber {
	
	private String phoneNumber;
	
	
	/**
	 * 
	 * @param emailAddress
	 */
	public PhoneNumber(final String phoneNumber) {
			this.phoneNumber = phoneNumber;
	}
	
	
	/**
	 * Overrides the toString()-Method to return the phoneNumber.
	 */
	@Override
	public String toString() {
		return this.phoneNumber;
	}
}