package view;


import java.util.ArrayList;
import java.util.List;

import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.AllContacts;
import model.Contact;
import model.PhoneNumber;

/**
 * 
 * @author Marc
 * 
 * 
 * Gibt es in JavaFX (wie z.B. mit Android) die Möglichkeit einzelne 
 * Fragmente mit einer FXML-Datei zu erstellen um dann in einer Schleife o.ä. zu sagen:
 * for (contact.length) {
 * 	  FXMLLoader.load("fragment.fxml")
 * }
 * ??? Ansonsten würde mich brennend interessieren wie ich diese Aufgabe mit einer
 * FXML Datei hätte lösen können. (Unter der Voraussetzung das eine variable Kontakt-Liste
 * vorliegt.
 * Im Zuge dieser EA habe ich im Internet danach geschaut aber nichts gefunden.
 * In Android kann man für jeden Listeneintrag in einer Listview xml-Fragment laden, welches
 * vorher mit xml gelayoutet wurde.
 *
 *
 */
public class ShowContacts extends Application {
	
	private final int WIDTH = 550;
	private final int HEIGHT = 400;


	/**
	 * JavaFX-Start-Method with the parameter Stage. This Method builds the 
	 * application and shows the primary Stage.
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		
		List<Contact> allContacts = AllContacts.getContactList();
		
		ScrollPane root = new ScrollPane();
		VBox box = new VBox();
		
		String css = this.getClass().getResource("application.css").toExternalForm(); 
		
		Scene scene = new Scene(root, WIDTH, HEIGHT);
		scene.getStylesheets().add(css);
		
		for (Contact contact: allContacts) {
			GridPane gridPane = showContact(contact);
			gridPane.getStyleClass().add("gridPane");
			box.getChildren().add(gridPane);
		}
		
		root.setContent(box);
		

		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	
	/**
	 * This method has a Parameter Contact. The method builds a GridPane
	 * with the Contact-Object and returns the Grid.
	 * @param Expects a Parameter from type Contact.
	 * @return Returns the created GridPane.
	 */
	private GridPane showContact(final Contact contact) {
		GridPane gridPane = new GridPane();
		ScrollPane emailScrollPane = showPhoneNumbers(contact);
		Image image;
		
		String nameString = "Vorname: " + contact.getFirstName() + "\n" +
								"Nachname: " + contact.getLastName() + "\n";
		
		if (contact.getEmail() != null) {
			nameString += "Email: " + contact.getEmail() + "\n";
		}
		
		String phoneNumberString = "";
		for (PhoneNumber phoneNumber: contact.getPhoneNumbersList()) {
			phoneNumberString += phoneNumber + "\n";
		}
		
		if (contact.getImage() != null) {
			image = new Image(getClass().getResourceAsStream(contact.getImage()));
		} else {
			image = new Image(getClass().getResourceAsStream("../resources/person.png"));
		}
		
		ImageView imageView = new ImageView(image);
		imageView.setFitWidth(80);
		imageView.setFitHeight(80);
		
		
		Label nameLabel = new Label(nameString);
		Label phoneLabel = new Label(phoneNumberString);
		
		
		emailScrollPane.setContent(phoneLabel);
		emailScrollPane.setPrefHeight(50);
		emailScrollPane.setPrefWidth(500);
		
		gridPane.setPadding(new Insets(15, 15, 15, 15));
		gridPane.setMinWidth(290);
		
		// phoneNumberScrollPane merges 2 columns
		GridPane.setHalignment(imageView, HPos.RIGHT);
		GridPane.setColumnSpan(emailScrollPane, 2);
		
		gridPane.add(nameLabel, 0, 1);
		gridPane.add(imageView, 1, 1);
		gridPane.add(emailScrollPane, 0, 2);
		return gridPane;
	}
	
	
	/**
	 * This method has a Parameter Contact. In a ScrollPane the number
	 * of Phonenumbers this contact has will be listed. 
	 * @param contact
	 * @return The ScrollPane with all Phonenumbers of this contact.
	 */
	private ScrollPane showPhoneNumbers(final Contact contact) {
		GridPane root = new GridPane();
		ArrayList<PhoneNumber> phoneNumbers = contact.getPhoneNumbersList();

		ScrollPane scrollPane = new ScrollPane();
		
		return scrollPane;
	}
	
	
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}

}
